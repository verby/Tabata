package fr.iureva.tabata;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.concurrent.TimeUnit;

import fr.iureva.tabata.data.State;
import fr.iureva.tabata.data.Tabata;


public class CounterActivity extends Activity implements SensorEventListener {
    private TextView textViewTime, textViewRounds, textViewWorkPreview, textViewRestPreview;
    private LinearLayout timeLeftPanel;
    private Button buttonRun, buttonReset, buttonSettings;
    private boolean running = false, prep;
    private String runTime, restTime,nameTabata;
    private long workTimeMillis, restTimeMillis, intervalMillis, prepTimeMillis, endRoundWarnMillis;
    Tabata selectedTabata;

    private State state = null;
    private Integer roundCurrent, roundsTotal;
    private Counter workTimer, restTimer, prepTimer;

    private SoundPool soundPool;
    private boolean loaded, alertPlayed, mute, proximity;
    private int soundAlertId, soundBellId;

    SharedPreferences sp;

    private Vibrator v;
    private long[] vPattern = {0, 300, 100, 300, 100, 300};
    private SensorManager sensorManager;
    private Sensor proximitySensor;

    // TempTimer object for pausing and resuming
    private long tempMillisLeft = 0;

    // SensorEventListener Override Methods
    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.values[0] == 0 && proximity) {
            if (prepTimer == null && roundCurrent == 1 && state == State.PREP) {
                prepTimer = new Counter(prepTimeMillis, intervalMillis);
                running = true;
                prepTimer.start();
                setTextViewTimeColor();
                buttonRun.setText(R.string.pause);
            } else if (workTimer == null && roundCurrent == 1 && state == State.WORK) {
                workTimer = new Counter(workTimeMillis, intervalMillis);
                running = true;
                workTimer.start();
                setTextViewTimeColor();
                buttonRun.setText(R.string.pause);
                playBellSoundVibrate();
            } else {
                if (!running) {
                    if (state == State.WORK) {
                        workTimerResume();
                    } else if (state == State.REST) {
                        restTimerResume();
                    } else if (state == State.PREP) {
                        prepTimerResume();
                    }
                } else {
                    if (state == State.WORK) {
                        workTimerPause();
                    } else if (state == State.REST) {
                        restTimerPause();
                    } else if (state == State.PREP) {
                        prepTimerPause();
                    }
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e("CounterActivity", "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counter);
        final Intent intent = getIntent();
        selectedTabata=(Tabata) intent.getSerializableExtra(MainActivity.ID_TABATA);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        sp = PreferenceManager.getDefaultSharedPreferences(this);

        // Sets the hardware button to control music volume
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);

        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                loaded = true;
            }
        });
        soundAlertId = soundPool.load(this, R.raw.bit_alert, 1);
        soundBellId = soundPool.load(this, R.raw.bit_bell, 1);

        roundCurrent = 1;

        // Preview work time and rest time settings
        textViewRestPreview = (TextView) findViewById(R.id.rest_preview_text);
        textViewWorkPreview = (TextView) findViewById(R.id.work_preview_text);

        // Load shared preferences
        loadPreferences(sp, selectedTabata);

        // Initialize Vibration on phone
        v = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
        timeLeftPanel = (LinearLayout) findViewById(R.id.time_panel);
        textViewTime = (TextView) findViewById(R.id.time_left_textView);

        textViewTime.setText(runTime);
        textViewRounds = (TextView) findViewById(R.id.round_number);
        setRoundTextView();
        buttonRun = (Button) findViewById(R.id.run_button);
        buttonRun.setText(R.string.work);
        buttonRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prepTimer == null && roundCurrent == 1 && state == State.PREP) {
                    prepTimer = new Counter(prepTimeMillis, intervalMillis);
                    running = true;
                    prepTimer.start();
                    setTextViewTimeColor();
                    buttonRun.setText(R.string.pause);
                } else if (workTimer == null && roundCurrent == 1 && state == State.WORK) {
                    workTimer = new Counter(workTimeMillis, intervalMillis);
                    running = true;
                    workTimer.start();
                    setTextViewTimeColor();
                    buttonRun.setText(R.string.pause);
                    playBellSoundVibrate();
                } else {
                    if (!running) {
                        if (state == State.WORK) {
                            workTimerResume();
                        } else if (state == State.REST) {
                            restTimerResume();
                        } else if (state == State.PREP) {
                            prepTimerResume();

                        }
                    } else {
                        if (state == State.WORK) {
                            workTimerPause();
                        } else if (state == State.REST) {
                            restTimerPause();
                        } else if (state == State.PREP) {
                            prepTimerPause();
                        }
                    }

                }
            }
        });
        buttonReset = (Button) findViewById(R.id.reset_button);
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (running) {
                    Toast.makeText(CounterActivity.this, R.string.stop_toast, Toast.LENGTH_SHORT).show();
                } else {
                    cancelNullTimers();
                    tempMillisLeft = workTimeMillis;
                    roundCurrent = 1;
                    setRoundTextView();
                    textViewTime.setText(runTime);
                    if (prep) {
                        state = State.PREP;
                    } else {
                        state = State.WORK;
                    }
                    buttonRun.setText(R.string.work);
                    running = false;
                    setTextViewTimeColor();
                }
            }
        });
        buttonSettings = (Button) findViewById(R.id.settings_button);
        buttonSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (running) {
                    Toast.makeText(CounterActivity.this, R.string.stop_toast, Toast.LENGTH_SHORT).show();
                } else {
                    Intent i = new Intent(CounterActivity.this, SettingsActivity.class);
                    //Put params
                    i.putExtra(MainActivity.ID_TABATA, selectedTabata);
                    startActivity(i);
                    finish();
                }
            }
        });
    }

    public void cancelNullTimers() {
        if (restTimer != null) {
            restTimer.cancel();
            restTimer = null;
        }
        if (workTimer != null) {
            workTimer.cancel();
            workTimer = null;
        }
        if (prepTimer != null) {
            prepTimer.cancel();
            prepTimer = null;
        }
    }

    @Override
    protected void onResume() {
        Log.e("CounterActivity", "onResume");
        super.onResume();
        if (proximity) {
            sensorManager.registerListener(this, proximitySensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onRestart() {
        Log.e("CounterActivity", "onRestart");
        super.onRestart();
    }

    @Override
    protected void onPause() {
        Log.e("CounterActivity", "onPause()");
        super.onPause();
        if (proximity) {
            sensorManager.unregisterListener(this);
        }
    }

    @Override
    public void finish() {
        cancelNullTimers();
        super.finish();
    }

    @Override
    protected void onDestroy() {
        Log.e("CounterActivity", "onDestroy()");
        finish();
        super.onDestroy();
    }@Override
    protected void onStop() {
        Log.e("CounterActivity", "onStop()");
        super.onStop();
    }

    public void cleanParams(Intent intent) {
        intent.putExtra(MainActivity.ID_TABATA, "Good job!");
        CounterActivity.this.setResult(1, intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void playAlertSound() {
        AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        float actualVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        float volume = actualVolume / maxVolume;
        if (loaded && !mute) {
            soundPool.play(soundAlertId, volume, volume, 1, 0, 1f);
        }
    }

    private void playBellSoundVibrate() {
        AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        float actualVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        float volume = actualVolume / maxVolume;
        if (loaded && !mute) {
            soundPool.play(soundBellId, volume, volume, 1, 0, 1f);
        }
        v.vibrate(vPattern, -1);
    }

    private void restTimerPause() {
        tempMillisLeft = restTimer.getMillisLeft();
        restTimer.cancel();
        buttonRun.setText(R.string.work);
        running = false;
        setTextViewTimeColor();
    }

    private void restTimerResume() {
        restTimer = new Counter(tempMillisLeft, intervalMillis);
        running = true;
        buttonRun.setText(R.string.pause);
        restTimer.start();
        setTextViewTimeColor(tempMillisLeft);
    }

    private void prepTimerPause() {
        tempMillisLeft = prepTimer.getMillisLeft();
        prepTimer.cancel();
        buttonRun.setText(R.string.work);
        running = false;
        setTextViewTimeColor();
    }

    private void prepTimerResume() {
        prepTimer = new Counter(tempMillisLeft, intervalMillis);
        running = true;
        buttonRun.setText(R.string.pause);
        prepTimer.start();
        setTextViewTimeColor();
    }

    private void workTimerPause() {
        tempMillisLeft = workTimer.getMillisLeft();
        workTimer.cancel();
        buttonRun.setText(R.string.work);
        running = false;
        setTextViewTimeColor();
    }

    private void workTimerResume() {
        workTimer = new Counter(tempMillisLeft, intervalMillis);
        running = true;
        buttonRun.setText(R.string.pause);
        workTimer.start();
        setTextViewTimeColor(tempMillisLeft);
    }

    private void setRoundTextView() {
        textViewRounds.setText(" " + roundCurrent + "/" + roundsTotal);
    }

    private void setTextViewTimeColor(long millis) {
        if (!running) {
            timeLeftPanel.setBackgroundResource(R.drawable.timer_bg_gray);
        } else if (running && state == State.WORK) {
            if (millis <= endRoundWarnMillis) {
                timeLeftPanel.setBackgroundResource(R.drawable.timer_bg_yellow);
            } else
                timeLeftPanel.setBackgroundResource(R.drawable.timer_bg_green);
        } else if (running && state == State.REST) {
            if (millis <= prepTimeMillis) {
                timeLeftPanel.setBackgroundResource(R.drawable.timer_bg_yellow);
            } else
                timeLeftPanel.setBackgroundResource(R.drawable.timer_bg_red);
        }
    }

    //
    private void setTextViewTimeColor() {
        // Changes the background instead
        if (!running) {
            timeLeftPanel.setBackgroundResource(R.drawable.timer_bg_gray);
        } else if (running && state == State.WORK) {
            timeLeftPanel.setBackgroundResource(R.drawable.timer_bg_green);
        } else if (running && state == State.REST) {
            timeLeftPanel.setBackgroundResource(R.drawable.timer_bg_red);
        } else if (running && state == State.PREP) {
            timeLeftPanel.setBackgroundResource(R.drawable.timer_bg_yellow);
        }
    }

    private void setTextViewTimeWarnPrep(long millis) {
        if (millis <= endRoundWarnMillis && state == State.WORK) {
            timeLeftPanel.setBackgroundResource(R.drawable.timer_bg_yellow);
        }
        if (millis <= prepTimeMillis && state == State.REST) {
            timeLeftPanel.setBackgroundResource(R.drawable.timer_bg_yellow);
        }
    }

    private void loadPreferences(SharedPreferences sp, Tabata newTabata) {
        proximity = sp.getBoolean("proximity_sensor_key", true);

        if (proximity) {
            sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
            proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        }
        workTimeMillis = TimePreference.getMillis(sp.getString("round_time_key", "3:00"));
        nameTabata=sp.getString("name_key", "Nom de Tabata");
        Log.d("LOOO NomTabata", newTabata.getNom());
        Log.d("LOOO NomDans Doc",     nameTabata);

        if(!nameTabata.equals(newTabata.getNom())){
            SharedPreferences.Editor editor = sp.edit();
            editor.putString( "round_time_key",  newTabata.getTravail());
            editor.putString( "rest_time_key", newTabata.getRepos());
            editor.putString( "name_key", newTabata.getNom());
            editor.putString( "number_rounds_key",  newTabata.getNbRepetition().toString());
            editor.commit();
            Log.d("LOOO ","here");
        }else{
            Log.d("LOOO","else");
            //Saving values in DB
            Log.d("LOOO ",sp.getString("number_rounds_key", "3:00"));
            Log.d("LOOO",selectedTabata.getNbRepetition()+"");
            Log.d("LOOO ",sp.getString("round_time_key", "1"));
            Log.d("LOOO",selectedTabata.getTravail());
            Log.d("LOOO",sp.getString("rest_time_key", "1:00"));
            Log.d("LOOO",selectedTabata.getRepos());


            selectedTabata.setNbRepetition(Integer.parseInt(sp.getString("number_rounds_key", "3:00")));
            selectedTabata.setTravail(sp.getString("round_time_key", "1"));
            selectedTabata.setRepos(sp.getString("rest_time_key", "1:00"));
            selectedTabata.save();
            Log.d("LOOO","lk,lk");
            //selectedTabata.setNom(sp.getString("name_key", "NomTabata"));
           // selectedTabata.save();

        }

        roundsTotal = Integer.parseInt(sp.getString("number_rounds_key", "12"));
        workTimeMillis = TimePreference.getMillis(sp.getString("round_time_key", "3:00"));
        restTimeMillis = TimePreference.getMillis(sp.getString("rest_time_key", "1:00"));
        textViewWorkPreview.setText("Work: " + sp.getString("round_time_key", "work time"));
        textViewRestPreview.setText("Rest: " + sp.getString("rest_time_key", "rest time"));

        mute = sp.getBoolean("mute_key", false);
        intervalMillis = 1000;
        alertPlayed = false;
        prep = sp.getBoolean("prep_time_key", true);
        prepTimeMillis = 10000;
        if (prep) {
            state = State.PREP;
        } else {
            state = State.WORK;
        }
        endRoundWarnMillis = Long.parseLong(sp.getString("warn_time_key", "20")) * 1000;
        runTime = String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(workTimeMillis),
                TimeUnit.MILLISECONDS.toSeconds(workTimeMillis) % 60);
        restTime = String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(restTimeMillis),
                TimeUnit.MILLISECONDS.toSeconds(restTimeMillis) % 60);

        //
    }

    // Counter CLASS Section
    private class Counter extends CountDownTimer {
        private String ms;
        private long millisLeft;
        private long mins, secs;

        public Counter(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            if (state == State.PREP) {
                if (prepTimer != null) {
                    prepTimer = null;
                }
                state = State.WORK;
                ms = runTime;
                workTimer = new Counter(workTimeMillis, intervalMillis);
                workTimer.start();
                playBellSoundVibrate();
                setTextViewTimeColor();
            } else if (state == State.REST && roundCurrent < roundsTotal) {
                ++roundCurrent;
                if (restTimer != null) {
                    restTimer.cancel();
                    restTimer = null;
                }
                state = State.WORK;
                ms = runTime;
                setRoundTextView();
                setTextViewTimeColor();
                workTimer = new Counter(workTimeMillis, intervalMillis);
                workTimer.start();
                alertPlayed = false;
                playBellSoundVibrate();
            } else if (state == State.WORK && roundCurrent < roundsTotal) {
                if (workTimer != null) {
                    workTimer.cancel();
                    workTimer = null;
                }
                if (restTimeMillis > 0) {
                    state = State.REST;
                    setRoundTextView();
                    setTextViewTimeColor();
                    ms = restTime;
                    restTimer = new Counter(restTimeMillis, intervalMillis);
                    restTimer.start();
                    alertPlayed = false;
                    playBellSoundVibrate();
                } else {
                    ++roundCurrent;
                    state = State.WORK;
                    ms = runTime;
                    setRoundTextView();
                    setTextViewTimeColor();
                    workTimer = new Counter(workTimeMillis, intervalMillis);
                    workTimer.start();
                    alertPlayed = false;
                    playBellSoundVibrate();
                }
            } else if (roundCurrent == roundsTotal) {

                textViewTime.setText("DONE!");
                state = State.DONE;
                alertPlayed = false;
                playBellSoundVibrate();
                setRoundTextView();
                setTextViewTimeColor();
                running = false;
                cancelNullTimers();
                ms = runTime;
            }


        }

        @Override
        public void onTick(long millisUntilFinished) {
            millisLeft = millisUntilFinished;
            mins = TimeUnit.MILLISECONDS.toMinutes(millisLeft);
            secs = TimeUnit.MILLISECONDS.toSeconds(millisLeft) % 60;
            ms = String.format("%02d:%02d", mins, secs);
            setTextViewTimeWarnPrep(millisLeft);
            // System.out.println(ms);
            if (millisLeft <= endRoundWarnMillis && !alertPlayed && state == State.WORK
                    || (millisLeft <= prepTimeMillis && state == State.REST && !alertPlayed)) {
                playAlertSound();
                alertPlayed = true;
            }
            textViewTime.setText(ms);
        }

        public long getMillisLeft() {
            return millisLeft;
        }
    }

}
