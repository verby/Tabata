package fr.iureva.tabata;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;

import fr.iureva.tabata.data.Tabata;


public class NewTabataActivity extends AppCompatActivity {

    private EditText nom, travail, repos, nbRepetition;
    Button saveButton;
    private TimePicker picker = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_tabata);
        nom = (EditText) findViewById(R.id.nom);
        travail = (EditText) findViewById(R.id.travail);
        repos = (EditText) findViewById(R.id.repos);
        nbRepetition = (EditText) findViewById(R.id.nbRepetition);

        //Disable saveButton
        saveButton = new Button(this);
        saveButton = findViewById(R.id.buttonSaveTabata);
        saveButton.setEnabled(false);
        //Listerners
        nom.addTextChangedListener(watcher);
        travail.addTextChangedListener(watcher);
        repos.addTextChangedListener(watcher);
        nbRepetition.addTextChangedListener(watcher);


        //TimePickers
        travail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(NewTabataActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        travail.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);
                mTimePicker.setTitle(R.string.round_time_title);
                mTimePicker.show();

            }
        });

        repos.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(NewTabataActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        repos.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);
                mTimePicker.setTitle(R.string.rest_time_title);
                mTimePicker.show();

            }
        });

    }

    public void saveNewTabata(View view) {

        String nomTabata = nom.getText().toString();
        String travailTabata = travail.getText().toString();
        String reposTabata = repos.getText().toString();
        Integer nbTabata = Integer.parseInt(nbRepetition.getText().toString());

        Tabata newTabata = new Tabata(nomTabata, travailTabata, reposTabata, nbTabata);
        newTabata.save();

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void cancelTabata(View view) {
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private final TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (!nom.getText().toString().isEmpty() && !repos.getText().toString().isEmpty() && !nbRepetition.getText().toString().isEmpty() && !travail.getText().toString().isEmpty()&&Integer.parseInt(nbRepetition.getText().toString())!=0) {
                saveButton.setEnabled(true);
            } else {
                saveButton.setEnabled(false);
            }
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}