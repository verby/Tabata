package fr.iureva.tabata;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import fr.iureva.tabata.data.SetTabatas;



public class NewSetActivity extends AppCompatActivity {

    private EditText nom, reposLong, nbRepetition;
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_set);

        nom = (EditText) findViewById(R.id.nom);
        reposLong = (EditText) findViewById(R.id.reposLong);
        nbRepetition = (EditText) findViewById(R.id.nbRepetition);

        //Disable saveButton
        saveButton = new Button(this);
        saveButton = findViewById(R.id.buttonSaveSet);
        saveButton.setEnabled(false);
        //Listerners
        nom.addTextChangedListener(watcher);
        reposLong.addTextChangedListener(watcher);
        nbRepetition.addTextChangedListener(watcher);

    }

    public void saveNewSet(View view) {

        String nomSet = nom.getText().toString();
        Integer reposLongSet = Integer.parseInt(reposLong.getText().toString());
        Integer nbSet = Integer.parseInt(nbRepetition.getText().toString());

        SetTabatas newSetTabatas = new SetTabatas(nomSet, reposLongSet, nbSet,null,null,null);
        newSetTabatas.save();

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void annulerSet(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


    private final TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {}

        @Override
        public void afterTextChanged(Editable editable) {
            if (!nom.getText().toString().isEmpty()  && !reposLong.getText().toString().isEmpty()&& !nbRepetition.getText().toString().isEmpty()) {
                saveButton.setEnabled(true);
            } else {
                saveButton.setEnabled(false);
            }

        }
    };
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }


}