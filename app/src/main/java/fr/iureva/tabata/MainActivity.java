package fr.iureva.tabata;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import java.util.List;
import fr.iureva.tabata.data.SetDao;
import fr.iureva.tabata.data.SetTabatas;
import fr.iureva.tabata.data.Tabata;
import fr.iureva.tabata.data.TabataDao;

public class MainActivity extends FragmentActivity {

    private LinearLayout layoutView;
    private List<Tabata> listTabatas;
    public final static String ID_TABATA = "idTabata";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listElements();
    }

    private void deleteTabata(final Tabata tab) {
        //AlertDialog
        AlertDialog.Builder alertBuild = new AlertDialog.Builder(this);

        alertBuild.setTitle(R.string.delete);
        alertBuild.setMessage("Voulez-vous supprimer la Tabata "+tab.getNom()+"?");

        final Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        alertBuild.setPositiveButton(R.string.set, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        tab.delete();
                        startActivity(intent);
                    }
                }
        );
        alertBuild.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }
        );

        AlertDialog alert = alertBuild.create();
        alert.show();
    }


    public void listElements() {
        listTabatas = TabataDao.selectAll();

        //Liste des SetTabatas
        final List<SetTabatas> listSets = SetDao.selectAll();

        //Clean results
        layoutView = (LinearLayout) findViewById(R.id.listTabatas);
        layoutView.removeAllViewsInLayout();

        //Add lines
        for (final Tabata t : listTabatas) {
            //Initialisation
            LinearLayout lineTabata = new LinearLayout(this);
            Button nomTabata = new Button(this);
            //Button style
            nomTabata.setBackgroundResource(R.drawable.my_button_bg);
            nomTabata.setWidth(700);
            nomTabata.setText(t.getNom());

            //Go to Counter
            nomTabata.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intentCounter = new Intent(MainActivity.this,
                            CounterActivity.class);
                    //Put params
                    intentCounter.putExtra(ID_TABATA, t);
                    startActivityForResult(intentCounter, 0);
                }
            });

            //Delete Tabata
            nomTabata.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    deleteTabata(t);
                    return true;
                }
            });
            //Add to Laylout
            lineTabata.addView(nomTabata);
            //Add to View
            layoutView.addView(lineTabata);
        }
    }


    public void newTabata(View view) {
        Intent intent = new Intent(this, NewTabataActivity.class);
        startActivity(intent);
    }

    //Création de SetTabatas
    public void newSet(View view) {
        Intent intent = new Intent(this, NewSetActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        listElements();
        super.onResume();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    protected void onStop() {
        super.onStop();
    }

}
