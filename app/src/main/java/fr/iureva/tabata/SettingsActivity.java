package fr.iureva.tabata;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.KeyEvent;

import fr.iureva.tabata.data.Tabata;

public class SettingsActivity extends PreferenceActivity {
    private boolean sendingMail = false;
    private Tabata selectedTabata;

    @SuppressLint("ResourceType")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Intent intent = getIntent();
        selectedTabata = new Tabata();
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        selectedTabata = (Tabata) intent.getSerializableExtra(MainActivity.ID_TABATA);
        addPreferencesFromResource(R.xml.preferences);

        Preference mailTo = (Preference) findPreference("mailTo");

        mailTo.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent mailto = new Intent(Intent.ACTION_SEND);
                mailto.setType("message/rfc822");
                mailto.putExtra(Intent.EXTRA_EMAIL, new String[]{"nadezhda@iureva.fr"});
                mailto.putExtra(Intent.EXTRA_SUBJECT, "TabataApp");
                mailto.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(mailto, "Choisir courriel"));
                sendingMail = true;
                return true;
            }
        });
    }


    protected void onResume(int keyCode, KeyEvent event) {
        sendingMail = false;
        super.onResume();
    }


    @Override
    protected void onStop() {
        if (!sendingMail) {
        Intent i = new Intent(this, CounterActivity.class);
        //Put params
        i.putExtra(MainActivity.ID_TABATA, selectedTabata);
         startActivity(i);
        }
        super.onStop();
    }

}
