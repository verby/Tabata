package fr.iureva.tabata.data;

import com.orm.SugarRecord;
import com.orm.dsl.Table;
import java.util.List;

@Table
public class SetTabatas extends SugarRecord {
    private String nom;
    private Integer reposLong;
    private Integer nbRepetition;
    private Integer ordre;
    private Integer groupe;
    private Integer tabFK;

    public SetTabatas(){}

    public SetTabatas(String nom, Integer reposLong, Integer nbRepetition, Integer ordre, Integer groupe, Integer tabFK) {
        this.nom = nom;
        this.reposLong = reposLong;
        this.nbRepetition = nbRepetition;
        this.ordre = ordre;
        this.groupe = groupe;
        this.tabFK = tabFK;
    }

    @Override
    public String toString() {
        return "SetTabatas{" +
                "nom='" + nom + '\'' +
                ", reposLong=" + reposLong +
                ", nbRepetition=" + nbRepetition +
                '}';
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getReposLong() {
        return reposLong;
    }

    public void setReposLong(Integer reposLong) {
        this.reposLong = reposLong;
    }

    public Integer getNbRepetition() {
        return nbRepetition;
    }

    public void setNbRepetition(Integer nbRepetition) {
        this.nbRepetition = nbRepetition;
    }

    public Integer getOrdre() {
        return ordre;
    }

    public void setOrdre(Integer ordre) {
        this.ordre = ordre;
    }

    public Integer getGroupe() {
        return groupe;
    }

    public void setGroupe(Integer groupe) {
        this.groupe = groupe;
    }

    public Integer getTabFK() {
        return tabFK;
    }

    public void setTabFK(Integer tabFK) {
        this.tabFK = tabFK;
    }

}
