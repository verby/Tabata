package fr.iureva.tabata.data;

import android.os.CountDownTimer;

import com.orm.SugarRecord;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

import java.io.Serializable;
import java.util.List;

@Table
public class Tabata extends SugarRecord implements Serializable {
    private String nom;
    private String travail;
    private String repos;
    private Integer nbRepetition;

    public Tabata(){}

    public Tabata(String nom, String travail, String repos, Integer nbRepetition) {
        this.nom = nom;
        this.travail = travail;
        this.repos = repos;
        this.nbRepetition = nbRepetition;
    }

    @Override
    public String toString() {
        return "Tabata{" +
                "nom='" + nom + '\'' +
                ", travail='" + travail + '\'' +
                ", repos='" + repos + '\'' +
                ", nbRepetition=" + nbRepetition +
                '}';
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTravail() {
        return travail;
    }

    public void setTravail(String travail) {
        this.travail = travail;
    }

    public String getRepos() {
        return repos;
    }

    public void setRepos(String repos) {
        this.repos = repos;
    }

    public Integer getNbRepetition() {
        return nbRepetition;
    }

    public void setNbRepetition(Integer nbRepetition) {
        this.nbRepetition = nbRepetition;
    }
}
