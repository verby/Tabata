package fr.iureva.tabata.data;
import java.util.List;

public class TabataDao {

   public static List<Tabata> selectAll() {

       return Tabata.listAll(Tabata.class);
    }

    public static Tabata findById(Long id){
        Tabata tabata = Tabata.findById(Tabata.class, id);
       return tabata;
    }

}
