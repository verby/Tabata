package fr.iureva.tabata.data;

import java.util.List;

public class SetDao {

    public static List<SetTabatas> selectAll() {
        return SetTabatas.listAll(SetTabatas.class);
    }

    public static void deleteAll(){
        SetTabatas.deleteAll(SetTabatas.class);
    }

}
