package fr.iureva.tabata.data;

public enum State {
    REST, WORK, PREP, DONE
}